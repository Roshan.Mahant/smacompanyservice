package com.access.sma.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity
@Table(name="company_tab")
@DynamicUpdate
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="com_id_col")
	private Long id;
	
	@Column(name="com_name_col")
	private String name;
	
	@Column(name="com_regid_col")
	private String cregId;
	
	@Column(name="com_type_col")
	private String ctype;
	
	@Column(name="com_parent_col")
	private String parentOrg;
	
	@Column(name="com_mode_col")
	private String modeOfOperate;
	
	@Column(name="com_service_code_col")
	private String serviceCode;
	
	@OneToOne(
			cascade = CascadeType.ALL,
			fetch=FetchType.EAGER)  //when we save/update/delete company automatically Address also get saved
	                                //eager-when we select parent then child object gets selected, if we don't want child then lazy
	@JoinColumn(name="apiFK")
	private Address addr;
	
	
}

package com.access.sma.service;

import java.util.List;

import com.access.sma.entity.Company;

public interface ICompanyService {

	Long createCompany(Company cob);
	void updateCompany(Company cob);
	Company getOneComapny(Long id);
	List<Company> getAllCompanies();
	
}

package com.access.sma.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.access.sma.entity.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	
}

package com.access.sma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import lombok.extern.slf4j.Slf4j;


@SpringBootApplication
@EnableEurekaClient
public class SmaCompanyServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaCompanyServiceApplication.class, args);
	}

}
